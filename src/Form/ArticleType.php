<?php

namespace App\Form;


use App\Entity\Article;
use App\Entity\Categorie;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder

            # Champ Titre
            ->add('titre', TextType::class, [
                'required'      => true,
                'attr'  => [
                    'class'         => 'form-control',
                    'placeholder'   => 'Titre de l\'Article'
                ]
            ])

            # Champ Catégorie
            ->add('categorie', EntityType::class, [
                'class' => Categorie::class,
                'choice_label'  => 'libelle',
                'multiple'      => false,
                'expanded'      => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])

            # Champ Contenu
            ->add('contenu', TextareaType::class, [
                'required'  => true,
                'attr'      => [
                    'class' => 'form-control'
                ]
            ])

            # Featured Image
            ->add('featuredImage', FileType::class, [
                'required'  => false,
                'attr'      => [
                    'class' => 'dropify'
                ]
            ])

            # Special et Spotlight
            ->add('special', CheckboxType::class, [
                'required'  => false
            ])
            ->add('spotlight', CheckboxType::class, [
                'required'  => false
            ])

            # Submit
            ->add('submit', SubmitType::class, [
                'label' => 'Publier',
                'attr'  => [
                    'class' => 'btn btn-primary'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }

}