<?php
/**
 * Created by PhpStorm.
 * User: haroun
 * Date: 17/03/2018
 * Time: 15:47
 */

namespace App\Form;
use App\Entity\Box;
use App\Entity\Produit;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProduitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('NomProduit', TextType::class)
            ->add('prix', NumberType::class)
            ->add('submit', SubmitType::class, [
                'label' => 'Ajouter',
                'attr'  => [
                    'class' => 'btn btn-primary'
                ]
            ])
        ;
    }

            /**
             * @param OptionsResolver $resolver
             */
            public function configureOptions(OptionsResolver $resolver)
            {
                $resolver->setDefaults(array(
                    'data_class' => Produit::class,
                ));
            }

}