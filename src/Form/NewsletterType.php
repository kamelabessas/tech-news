<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 06/03/2018
 * Time: 12:40
 */

namespace App\Form;


use App\Entity\Newsletter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsletterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            # Champ Email
            ->add('email', EmailType::class, [
                'required'  => true,
                'label'     => false,
                'attr'      => [
                    'placeholder'   => 'Saisissez votre Email',
                    'class'         => 'form-control'
                ]
            ])

            # Bouton Submit
            ->add('submit', SubmitType::class, [
                'label'  => 'Je m\'inscris !',
                'attr'      => [
                    'class'   => 'btn btn-primary'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Newsletter::class
        ]);
    }
}