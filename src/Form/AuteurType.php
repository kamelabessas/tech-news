<?php

namespace App\Form;


use App\Entity\Auteur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AuteurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            # Champ Prénom
            ->add('prenom', TextType::class, [
                'required'  => true,
                'attr'      => [
                    'placeholder'   => 'Saisissez votre Prénom'
                ]
            ])

            # Champ Nom
            ->add('nom', TextType::class, [
                'required'  => true,
                'attr'      => [
                    'placeholder'   => 'Saisissez votre Nom'
                ]
            ])

            # Champ Email
            ->add('email', EmailType::class, [
                'required'  => true,
                'attr'      => [
                    'placeholder'   => 'Saisissez votre Email'
                ]
            ])

            # Champ Mot de Passe
            ->add('password', PasswordType::class, [
                'required'  => true,
                'attr'      => [
                    'placeholder'   => '************'
                ]
            ])

            # Bouton Submit
            ->add('submit', SubmitType::class, [
                'label'  => 'M\'inscrire !'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Auteur::class
        ]);
    }

}