<?php

namespace App\Service\Article;


use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

class YamlProvider
{

    private $kernel;

    /**
     * ArticleProvider constructor.
     * @param $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * Retourne la liste des articles depuis le cache
     */
    public function getArticles()
    {
        return unserialize( file_get_contents(
            $this->kernel->getCacheDir() . '/yaml-articles.php' ) );
    }
}
