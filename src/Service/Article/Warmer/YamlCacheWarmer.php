<?php

namespace App\Service\Article\Warmer;


use Symfony\Component\HttpKernel\CacheWarmer\CacheWarmer;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

class YamlCacheWarmer extends CacheWarmer
{

    public function isOptional()
    {
        return false;
    }

    /**
     * Warms up the cache.
     *
     * @param string $cacheDir The cache directory
     */
    public function warmUp($cacheDir)
    {
        try {

            # Je récupère mes articles sous forme de array
            # depuis mon format YAML.
            $articles = Yaml::parseFile(__DIR__ . '/../articles.yaml');
            $this->writeCacheFile($cacheDir.'/yaml-articles.php',
                serialize($articles['data']) );

        } catch (ParseException $e) {

            printf('Unable to parse the YAML string: %s', $e->getMessage());

        }
    }
}