<?php

namespace App\Service\Article;


use App\Entity\Article;

interface ArticleRepositoryInterface
{
    /**
     * Permet de retourner un article sur la base
     * de son identifiant unique.
     * @param $id
     * @return Article|null
     */
    public function find($id): ?Article;

    /**
     * Retourne la liste de tous les articles.
     * @return mixed
     */
    public function findAll();

    # public function findBy();
    # public function findOneBy();

}