<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 07/03/2018
 * Time: 15:39
 */

namespace App\Service\Article;


use App\Entity\Article;
use App\Service\Article\Source\ArticleAbstractSource;

class ArticleCatalogue implements ArticleCatalogueInterface
{
    /**
     * Les différentes sources de données de mon Mediateur.
     * Ex. YamlSource, DoctrineSource, ApiSource, ...
     */
    private $sources;

    public function addSource(ArticleAbstractSource $source): void
    {
        $this->sources[] = $source;
    }

    public function setSources(iterable $sources): void
    {
        $this->sources = $sources;
    }

    public function getSources(): iterable
    {
        return $this->sources;
    }

    /**
     * Permet de retourner un article sur la base
     * de son identifiant unique.
     * @param $id
     * @return Article|null
     */
    public function find($id): ?Article
    {
        // TODO: Implement find() method.
    }

    /**
     * Retourne la liste de tous les articles.
     * @return mixed
     */
    public function findAll()
    {
        // TODO: Implement findAll() method.
    }
}