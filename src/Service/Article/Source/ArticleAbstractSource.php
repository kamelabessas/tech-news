<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 07/03/2018
 * Time: 15:48
 */

namespace App\Service\Article\Source;


use App\Service\Article\ArticleCatalogue;
use App\Service\Article\ArticleRepositoryInterface;

abstract class ArticleAbstractSource implements ArticleRepositoryInterface
{

    protected $catalogue;

    public function setCatalogue(ArticleCatalogue $catalogue)
    {
        $this->catalogue = $catalogue;
    }

}