<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 07/03/2018
 * Time: 15:56
 */

namespace App\Service\Article\Source;


use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;

class DoctrineSource extends ArticleAbstractSource
{

    private $em,
            $entity = Article::class;

    /**
     * DoctrineSource constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Permet de retourner un article sur la base
     * de son identifiant unique.
     * @param $id
     * @return Article|null
     */
    public function find($id): ?Article
    {
        return $this->em
            ->getRepository($this->entity)
            ->find($id);
    }

    /**
     * Retourne la liste de tous les articles.
     * @return mixed
     */
    public function findAll()
    {
        // TODO: Implement findAll() method.
    }
}