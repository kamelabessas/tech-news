<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 07/03/2018
 * Time: 15:55
 */

namespace App\Service\Article\Source;


use App\Entity\Article;
use App\Entity\Auteur;
use App\Entity\Categorie;
use App\Service\Article\YamlProvider;

class YamlSource extends ArticleAbstractSource
{

    private $yamlProvider, $articles;

    /**
     * YamlSource constructor.
     * @param $yamlProvider
     */
    public function __construct(YamlProvider $yamlProvider)
    {
        $this->yamlProvider = $yamlProvider;
        $this->articles = $yamlProvider->getArticles();
    }

    /**
     * Permet de retourner un article sur la base
     * de son identifiant unique.
     * @param $id
     * @return Article|null
     */
    public function find($id): ?Article
    {
        # Récupération de l'Article dans le tableau
        $articleId = array_column($this->articles, 'id');
        $key = array_search($id, $articleId);

        if(!$key) {
            return null;
        }

        # Ici, je récupère mon article depuis le tableau
        $tmp = (object) $this->articles[$key];

        # Construire l'objet Catégorie
        $categorie = new Categorie();
        $categorie->setId($tmp->categorie['id']);
        $categorie->setLibelle($tmp->categorie['libelle']);

        # Construction de l'objet Auteur
        $auteur = new Auteur();
        $auteur->setId($tmp->auteur['id']);
        $auteur->setPrenom($tmp->auteur['prenom']);
        $auteur->setNom($tmp->auteur['nom']);
        $auteur->setEmail($tmp->auteur['email']);

        # Construire l'objet Article
        $article = new Article();
        $article->setId($tmp->id);
        $article->setTitre($tmp->titre);
        $article->setContenu($tmp->contenu);
        $article->setFeaturedImage($tmp->featuredimage);
        $article->setAuteur($auteur);
        $article->setCategorie($categorie);
        $article->setSpotlight($tmp->spotlight);
        $article->setSpecial($tmp->special);
        $article->setDateCreation($tmp->datecreation);

        return $article;

    }

    /**
     * Retourne la liste de tous les articles.
     * @return mixed
     */
    public function findAll()
    {
        // TODO: Implement findAll() method.
    }
}