<?php

namespace App\Service\Twig;



use App\Controller\Helper;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Twig\Extension\AbstractExtension;

class AppExtension extends AbstractExtension
{

    use Helper;

    private $session;

    /**
     * AppExtension constructor.
     * @param $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function getFilters()
    {
        return [

            new \Twig_Filter('accroche', function($text) {

                # Supprimer toutes les balises HTML
                $string = strip_tags($text);

                # Si ma chaine de caractère est supérieur à 170
                # Je poursuis, sinon c'est inutile...
                if(strlen($string) > 170) :

                    # Je coupe ma chaine à 170
                    $stringCut = substr($string, 0, 170);

                    # Je m'assure que je ne coupe pas de mot
                    $string = substr($stringCut, 0, strrpos($stringCut, ' ')) . '...';

                endif;

                # On retourne l'accroche
                return $string;

            }), #Fin du Filtre Accroche

            new \Twig_Filter('slugify', function($text) {

                return $this->slugify($text);

            }) # -- Fin du Filtre Slugify

        ]; # -- Fin du Array

    } # -- Fin de getFilters

    public function getFunctions()
    {
        return [

            new \Twig_Function('isUserInvited', function() {

                return $this->session->get('inviteUser');

            })

        ];
    }


} # -- de notre classe
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
# -- Fin du monde...
