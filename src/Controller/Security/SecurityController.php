<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 05/03/2018
 * Time: 10:20
 */

namespace App\Controller\Security;


use App\Entity\Auteur;
use App\Form\AuteurType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{

    /**
     * Inscription d'un utilisateur
     * @Route("/inscription", name="security_inscription",
     *     methods={"GET","POST"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function inscription(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        # Création d'un nouvel Auteur
        $auteur = new Auteur();
        $auteur->setRoles('ROLE_MEMBRE');

        # Créer un Formulaire permettant l'ajout d'un Auteur
        $form = $this->createForm(AuteurType::class, $auteur);

        # Traitement des données POST
        $form->handleRequest($request);

        # Vérification des données du Formulaire
        if($form->isSubmitted() && $form->isValid()) :

            # Gestion du mot de passe
            $password = $passwordEncoder->encodePassword($auteur, $auteur->getPassword());
            $auteur->setPassword($password);

            # Insertion en BDD
            $em = $this->getDoctrine()->getManager();
            $em->persist($auteur);
            $em->flush();

            # Redirection sur la page de connexion
            return $this->redirect('connexion?inscription=success');

        endif;

        # Affichage du Formulaire dans la vue
        return $this->render('security/inscription.html.twig', [
            'form' => $form->createView()
        ]);

    }

    /**
     * Connexion d'un utilisateur
     * @Route("/connexion", name="security_connexion")
     * @param Request $request
     * @param AuthenticationUtils $authenticationUtils
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function connexion(Request $request, AuthenticationUtils $authenticationUtils)
    {
        # Récupération du message d'erreur s'il y en a un.
        $error = $authenticationUtils->getLastAuthenticationError();

        # Dernier email saisie par l'utilisateur.
        $lastEmail = $authenticationUtils->getLastUsername();

        # Affichage du Formulaire
        return $this->render('security/connexion.html.twig', array(
            'last_email' => $lastEmail,
            'error' => $error,
        ));
    }

}
