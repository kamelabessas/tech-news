<?php

namespace App\Controller\TechNews;


use App\Form\NewsletterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NewsletterController extends Controller
{
    public function newsletter() {
        # Création du Formulaire
        $form = $this->createForm(NewsletterType::class);

        # Affichage du Formulaire Newsletter
        return $this->render('newsletter/subscribe.html.twig',[
            'form' => $form->createView()
        ]);
    }
}