<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 26/02/2018
 * Time: 14:24
 */

namespace App\Controller\TechNews;


use App\Entity\Article;
use App\Entity\Categorie;
use App\Service\Article\YamlProvider;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    public function index(YamlProvider $articleProvider)
    {
        # Récupération des Articles depuis ArticleProvider
        $yamlArticles = $articleProvider->getArticles();
        dump($yamlArticles);

        # Récupération des Articles depuis la BDD
        $repository = $this->getDoctrine()
            ->getRepository(Article::class);

        $articles  = $repository->findAll();
        $spotlight = $repository->findSpotlightArticles();

        # Transmission à la vue
        return $this->render('index/index.html.twig', [
            'articles' => $articles,
            'spotlight' => $spotlight
        ]);
    }

    /**
     * @Route("/categorie/{libelle}",
     *     name="index_categorie",
     *     methods={"GET"},
     *     defaults={"libelle":"tout"},
     *     requirements={"libelle":"\w+"})
     * @param string $libelle
     * @return Response
     */
    public function categorie($libelle)
    {

        $categorie = $this->getDoctrine()
            ->getRepository(Categorie::class)
            ->findOneBy([
               'libelle' => $libelle
            ]);

        $articles = $categorie->getArticles();

        return $this->render('index/categorie.html.twig', [
            'articles' => $articles
        ]);
    }

    /**
     * @Route("/{categorie}/{slug}_{id}.html",
     *     name="index_article",
     *     requirements={"id":"\d+"})
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function article(Article $article)
    {
        # business/une-formation-symfony-a-paris_879464.html
        # $article = $this->getDoctrine()
        #     ->getRepository(Article::class)
        #     ->find($id);

        # Si aucun article n'est trouvé...
        if(!$article) :

            # On génère une exception
            # throw $this->createNotFoundException(
            #     'Nous n\'avons pas trouvé votre article ID: ' . $id
            # );

            # Ou, on peut rediriger l'utilisateur sur la page index.
            return $this->redirectToRoute('index',[],Response::HTTP_MOVED_PERMANENTLY);

        endif;

        /**
         * Lazy Loading et le Chargement des Related Objects
         * Il est important de comprendre que nous avons accès à l'objet catégorie
         * de l'article de façon AUTOMATIQUE ! Cependant, les données de la catégorie
         * ne sont récupérés par doctrine que lorsque nous faisons la demande, et pas avant !
         * Ceci pour alléger le chargement de votre page !
         */
        # $categorie = $article->getCategorie()->getLibelle();

        # Récupération des suggestions
        $suggestions = $this->getDoctrine()
            ->getRepository(Article::class)
            ->findArticleSuggestions($article->getId(), $article->getCategorie()->getId());

        return $this->render('index/article.html.twig', [
            'article' => $article,
            'suggestions' => $suggestions
            #'categorie' => $categorie
        ]);
    }

    public function sidebar() {

        # Récupération du Repository
        $repository = $this->getDoctrine()->getRepository(Article::class);

        # Récupération des 5 derniers articles
        $articles = $repository->findLastFiveArticles();

        # Récupération des articles à la position "special"
        $specials = $repository->findSpecialArticles();

        return $this->render('components/_sidebar.html.twig', [
            'articles'  => $articles,
            'specials'   => $specials
        ]);

    }

}