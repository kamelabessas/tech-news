<?php

namespace App\Controller\TechNews;


use App\Controller\Helper;
use App\Entity\Article;
use App\Entity\Auteur;
use App\Entity\Categorie;
use App\Form\ArticleType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends Controller
{

    use Helper;

    /**
     * Démonstration de l'ajout d'un Article avec Doctrine
     * @Route("/article", name="article")
     */
    public function index()
    {
        # Création de la catégorie
        $categorie = new Categorie();
        $categorie->setLibelle('Business');

        # Création de l'Auteur
        $auteur = new Auteur();
        $auteur->setPrenom('Hugo');
        $auteur->setNom('LIEGEARD');
        $auteur->setEmail('wf3@hl-media.fr');
        $auteur->setPassword('MonSuperMotDePasse');

        # Création de l'Article
        $article = new Article();
        $article->setTitre('Tip Aligning Digital Marketing with Business Goals and Objectives');
        $article->setContenu('<p>CONTENU</p>');
        $article->setFeaturedImage('3.jpg');
        $article->setSpecial(0);
        $article->setSpotlight(1);

        # On associe une catégorie et un auteur à notre article
        $article->setAuteur($auteur);
        $article->setCategorie($categorie);

        # On sauvegarde le tout en BDD avec Doctrine
        $em = $this->getDoctrine()->getManager();
        $em->persist($categorie);
        $em->persist($auteur);
        $em->persist($article);
        $em->flush();

        # Retour à la Vue
        return new Response('Nouvel article ajouté avec ID : ' .
            $article->getId() . ' et la nouvelle catégorie ' .
            $categorie->getLibelle() . ' de Auteur : ' .
            $auteur->getPrenom());

    }

    /**
     * @Route("/creer-un-article", name="article_add")
     * @Security("has_role('ROLE_AUTEUR')")
     * @param Request $request
     * @return Response
     */
    public function addarticle(Request $request) {

        # Création d'un nouvel article
        $article = new Article();

        # Auteur
        $auteur = $this->getDoctrine()
            ->getRepository(Auteur::class)
            ->find(1);

        # Affecter mon auteur à l'article
        $article->setAuteur($auteur);

        # Créer un Formulaire permettant l'ajout d'un article
        $form = $this->createForm(ArticleType::class, $article);

        # Traitement des données POST
        $form->handleRequest($request);

        # Vérification des données du Formulaire
        if($form->isSubmitted() && $form->isValid()) :

            # Récupération des données
            $article = $form->getData();

            # Récupération de l'image
            $featuredimage = $article->getFeaturedImage();

            # Nom du Fichier
            $fileName = $this->slugify($article->getTitre()).'.'.
                $featuredimage->guessExtension();

            # Déplacer le fichier uploadé
            $featuredimage->move(
                $this->getParameter('articles_assets_dir'),
                $fileName
            );

            # Mettre à jour le nom de l'image
            $article->setFeaturedImage($fileName);

            # Insertion en BDD
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            # Redirection sur l'Article qui vient d'être crée.
            return $this->redirectToRoute('index_article', [
                'categorie'  => $this->slugify($article->getCategorie()->getLibelle()),
                'slug'      => $this->slugify($article->getTitre()),
                'id'        => $article->getId()
            ]);

        endif;

        # Affichage de mon formulaire dans la vue
        return $this->render('article/ajouterarticle.html.twig',[
            'form' => $form->createView()
        ]);

    }

}
