<?php
/**
 * Created by PhpStorm.
 * User: bessaskamela
 * Date: 02/07/2018
 * Time: 12:06
 */

namespace App\Controller;
use App\Entity\Article;
use App\SiteConfig;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Workflow\Exception\LogicException;
use Symfony\Component\Workflow\Registry;


class WorkflowController extends Controller
{


    /**
     * Afficher les articles d'un auteur
     * @Route(
     *     "/{_locale}/edition/my-articles/{currentPage}.html", name="author_articles_published", defaults={"currentPage" : 1})
     * @security("has_role('ROLE_AUTHOR')")
     *
     * @param string $currentPage
     * @return Response
     */
    public function authorArticles(string $currentPage)
    {
        #Récuperé l'utilisateur
        $author = $this->getUser();

        #Récupéré l'article
        $articles = $this->getDoctrine()->getRepository(Article::class)
            ->findAuthorArticlesByStatus($author->getId(), 'published');

        # Nombre d'artiles
        $countArticle = count($articles);

        #Obtenir seulement des articles recherchés
        $articles = array_slice($articles, ($currentPage - 1) * SiteConfig::NBARTICLEPERPAGE, SiteConfig::NBARTICLEPERPAGE);

        #Nombre de pagination
        $countPagination = ceil($countArticle / SiteConfig::NBARTICLEPERPAGE);

        #Affichage
        return $this->render('index/auteur.html.twig', array(
            'articles' => $articles,
            'author' => $author,
            'currentPage' => 1,
            'countPagination' => $countPagination
        ));

    }

    /**
     * Afficher les articles d'un auteur
     * @Route(
     *     "/{_locale}/edition/my-pending-articles/{currentPage}.html",
     *     name="author_articles_pending",
     *     defaults={"currentPage" : 1}
     * )
     * @Security("has_role('ROLE_AUTHOR')")
     *
     * @param string $currentPage
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function authorArticlesPending(string $currentPage)
    {
        #Récupéré l'auteur
        $author = $this->getUser();

        #Récupéré l'article
        $articles = $this->getDoctrine()->getRepository(Article::class)
            ->findAuthorArticlesByStatus($author->getId(), 'review');


        #Compter le nombre d'articles
        $countArticle = count($articles);

        #Obtenir seulement des articles recherchés
        $articles = array_slice($articles, ($currentPage - 1) * SiteConfig::NBARTICLEPERPAGE, SiteConfig::NBARTICLEPERPAGE);

        #Nombre de pagination
        $countPagination = ceil($countArticle / SiteConfig::NBARTICLEPERPAGE);

        #Affichage
        return $this->render('index/auteur.html.twig', array(
            'articles' => $articles,
            'author' => $author,
            'currentPage' => 1,
            'countPagination' => $countPagination
        ));
    }

    /**
     * Afficher les articles d'un auteur
     * @Route(
     *     "/{_locale}/edition/my-approval-articles/{currentPage}.html",
     *     name="author_articles_approval",
     *     defaults={"currentPage" : 1}
     * )
     * @Security("has_role('ROLE_EDITOR')")
     *
     * @param string $currentPage
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function articlesApproval(string $currentPage)
    {
        #Recupéré l'utilisateur
        $author = $this->getUser();

        #Recupéré l'article
        $articles = $this->getDoctrine()->getRepository(Article::class)
            ->findArticlesByStatus('editor');

        #Calculer le nombre d'articles
        $countArticle = count($articles);

        #Obtenir seulement des articles recherchés
        $articles = array_slice($articles, ($currentPage - 1) * SiteConfig::NBARTICLEPERPAGE, SiteConfig::NBARTICLEPERPAGE);

        #Nombre de pagination
        $countPagination = ceil($countArticle / SiteConfig::NBARTICLEPERPAGE);

        #Affichage
        return $this->render('index/auteur.html.twig', array(
            'articles' => $articles,
            'author' => $author,
            'currentPage' => 1,
            'countPagination' => $countPagination
        ));
    }

    /**
     * Recupéré les articles d'un auteur
     * @Route(
     *     "/{_locale}/edition/my-corrector-articles/{currentPage}.html",
     *     name="author_articles_corrector",
     *     defaults={"currentPage" : 1}
     * )
     * @Security("has_role('ROLE_CORRECTOR')")
     *
     * @param string $currentPage
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function articlesCorrector(string $currentPage)
    {
        #Récupéré l'utilisateur
        $author = $this->getUser();

        #Récupéré l'article
        $articles = $this->getDoctrine()->getRepository(Article::class)
            ->findArticlesByStatus('corrector');

        #Compter le nombre d'articles
        $countArticle = count($articles);

        #Obtenir seulement des articles recherchés
        $articles = array_slice($articles, ($currentPage - 1) * SiteConfig::NBARTICLEPERPAGE, SiteConfig::NBARTICLEPERPAGE);

        #Nombre de pagination
        $countPagination = ceil($countArticle / SiteConfig::NBARTICLEPERPAGE);

        #Affichage
        return $this->render('index/auteur.html.twig', array(
            'articles' => $articles,
            'author' => $author,
            'currentPage' => 1,
            'countPagination' => $countPagination
        ));
    }

    /**
     *
     * @Route(
     *     "/{_locale}/edition/my-publisher-articles/{currentPage}.html",
     *     name="author_articles_publisher",
     *     defaults={"currentPage" : 1}
     * )
     * @Security("has_role('ROLE_PUBLISHER')")
     *
     * @param string $currentPage
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function articlesPublisher(string $currentPage)
    {

        $author = $this->getUser();


        $articles = $this->getDoctrine()->getRepository(Article::class)
            ->findArticlesByStatus('publisher');


        $countArticle = count($articles);


        $articles = array_slice($articles, ($currentPage - 1) * SiteConfig::NBARTICLEPERPAGE, SiteConfig::NBARTICLEPERPAGE);


        $countPagination = ceil($countArticle / SiteConfig::NBARTICLEPERPAGE);


        return $this->render('index/auteur.html.twig', array(
            'articles' => $articles,
            'author' => $author,
            'currentPage' => 1,
            'countPagination' => $countPagination
        ));
    }


    /**
     * @Route(
     *     "/{_locale}/edition/workflow/{action}/{id}.html",
     *      name="workflow_action"
     * )
     * @Security("has_role('ROLE_AUTHOR')")
     *
     * @param string $action
     * @param String $id
     * @param Registry $workflows
     * @param Request $request
     * @return Response
     */
    public function workflowAction(string $action, String $id, Registry $workflows, Request $request): Response
    {
        #Verifier si l'article existe
        $article = $this
            ->getDoctrine()
            ->getRepository(Article::class)

            ->findOneBy(['id' => $id]);

        if (!$article) {
            #Si l'article n'existe pas
            $this->addFlash('error', "Cet article n'existe pas" );
        }

        $workflow = $workflows->get($article);

        #Si l'article existe
        # -------------
        if ($workflow->can($article, $action)) {
            try {
                #Application de la transition
                $workflow->apply($article, $action);

                #Insétion dans la base de données
                $eManager = $this->getDoctrine()->getManager();
                $eManager->persist($article);
                $eManager->flush();



                $this->addFlash('success', " L'article a bien été envoyé à l'éditeur !");

            } catch (LogicException $exception) {

                $this->addFlash('error', "Une erreur s'est produite lors de l'envoie de l'article " . $exception->getMessage());
            }
        } else {

            $this->addFlash('error', 'cannot ');
        }

        # Verifier si l'article peut étre publier
        if($workflow->can($article , 'publish')){
            $workflow->apply($article,"publish");
            #Insertion dans la base de données
            $eManager = $this->getDoctrine()->getManager();
            $eManager->persist($article);
            $eManager->flush();
        }

        # Obtenir la redirection
        $redirect = $request->get('redirect') ?? 'index';

        # redirection
        return $this->redirectToRoute($redirect);
    }


}