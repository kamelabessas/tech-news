<?php

namespace App\EventSubscriber;


use App\Entity\Auteur;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class AuteurSubscriber implements EventSubscriberInterface
{

    private $manager;

    /**
     * AuteurSubscriber constructor.
     * @param $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }


    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {

        # Récupération de l'Objet Auteur
        $auteur = $event->getAuthenticationToken()->getUser();

        # Mettre à jour la date de dernière connexion.
        if($auteur instanceof Auteur) {

            # Mise à jour du Timestamp
            $auteur->setDerniereConnexion();

            # Sauvegarde en BDD
            $this->manager->flush();

        }

    }

    public static function getSubscribedEvents()
    {
        return [
            SecurityEvents::INTERACTIVE_LOGIN => 'onSecurityInteractiveLogin'
        ];
    }
}