<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 06/03/2018
 * Time: 14:29
 */

namespace App\EventSubscriber;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class NewsletterSubscriber implements EventSubscriberInterface
{

    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        # On s'assure que la requête viens de l'utilisateur et non de Symfony !
        if(!$event->isMasterRequest() || $event->getRequest()->isXmlHttpRequest()) {
            return;
        }

        # On propose à l'utilisateur, de s'inscrire à la newsletter
        # au bout de la troisième page visitée.
        $this->session->set('countUserView',
            $this->session->get('countUserView', 0) + 1);

        # Inviter notre utilisateur au bout de la 3ème page consulté
        if($this->session->get('countUserView') === 3) :
            $this->session->set('inviteUser', true);
        endif;
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        # On s'assure que la requête viens de l'utilisateur et non de Symfony !
        if(!$event->isMasterRequest() || $event->getRequest()->isXmlHttpRequest()) {
            return;
        }

        # On passe à false l'inviteUser.
        if($this->session->get('countUserView') === 3) :
            $this->session->set('inviteUser', false);
        endif;

    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST  => 'onKernelRequest',
            KernelEvents::RESPONSE => 'onKernelResponse'
        ];
    }
}